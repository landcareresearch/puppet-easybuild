# EasyBuild Puppet Module

Needs a rewrite.

Forked from [sylmarien/puppet-easybuild](https://github.com/sylmarien/puppet-easybuild)

## Overview

This is an all in one module that installs easybuild and manages software installed by easy build.

## More details

http://easybuild.readthedocs.io/en/latest/Installation.html

### After Install

```bash
module load EasyBuild
```

## Class Documentation

See REFERENCE.md
