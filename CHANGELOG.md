# easybuild Puppet Module Changelog

## 2021-02-26 Version 6.0.0

- Used PDK to ensure Puppet 6 compliance.
- Refactored Documentation.

## 2017-03-22 Version 0.1.6

- The git repo for the easybuild configuration was removed (3rd party).  
  So the 2 configuration files in the repo have been moved into this puppet module.
- Gemfile has been updated for puppet 4.
