# @summary Manages the configuration.
#
class easybuild::config {
  anchor{'easybuild::config::begin': }

  $install_dir    = $easybuild::params::install_dir
  $python         = $easybuild::params::python_path
  $softwares      = $easybuild::softwares
  $bootstrap_file = 'bootstrap_eb.py'
  $bootstrap_url  = "https://raw.githubusercontent.com/hpcugent/easybuild-framework/develop/easybuild/scripts/${bootstrap_file}"
  $mod_src        = "/bin/bash -c 'source ${easybuild::params::module_source}"

  # Initializes the environment with easy build environment variables.
  # Note, this should probably be moved into an array and use the
  # environment parameter in exec.  But the original author did it this way
  # and its too much trouble to correct this right now.
  # BUT NOTE, it should be corrected at some point.
  $var_src        = "/bin/bash -c 'source ${install_dir}/variables.sh"

  user { 'sw':
    ensure     => present,
    home       => '/home/sw',
    managehome => true,
    shell      => '/bin/bash',
  }

  file {$install_dir:
    ensure  => directory,
    require => User['sw'],
  }

  file {'/opt/apps':
    ensure  => directory,
    owner   => 'sw',
    require => File[$install_dir],
  }

  file { "${install_dir}/easybuild_config":
    ensure  => directory,
    require => File['/opt/apps'],
  }

  file { "${install_dir}/easybuild_config/easybuild.sh":
    ensure  => file,
    mode    => '0755',
    source  => 'puppet:///modules/easybuild/easybuild.sh',
    require => File["${install_dir}/easybuild_config"],
  }

  file { "${install_dir}/easybuild_config/libc6.preseed":
    ensure  => file,
    source  => 'puppet:///modules/easybuild/libc6.preseed',
    require => File["${install_dir}/easybuild_config/easybuild.sh"],
  }

  # Configure environment variables for the module command
  file { '/etc/profile.d/easybuild.sh':
    ensure  => file,
    owner   => 'sw',
    source  => "${install_dir}/easybuild_config/easybuild.sh",
    require => File["${install_dir}/easybuild_config/libc6.preseed"],
  }

  # wget
  wget::fetch { 'bootstrap':
    source      => $bootstrap_url,
    destination => "${install_dir}/easybuild_config/${bootstrap_file}",
    timeout     => 0,
    verbose     => false,
    require     => File['/etc/profile.d/easybuild.sh'],
  }


  # install easy build
  exec { 'install_easybuild':
    user    => 'sw',
    command => "${mod_src} && python ${bootstrap_file} /opt/apps/EasyBuild'",
    creates => '/opt/apps/EasyBuild',
    umask   => '022',
    cwd     => "${install_dir}/easybuild_config",
    require => Wget::Fetch['bootstrap'],
  }

  file { 'install.py':
    ensure  => present,
    path    => "${install_dir}/install.py",
    owner   => 'sw',
    mode    => '0755',
    source  => 'puppet:///modules/easybuild/install.py',
    require => Exec['install_easybuild'],
  }

  file { 'variables.sh':
    ensure  => present,
    path    => "${install_dir}/variables.sh",
    owner   => 'sw',
    mode    => '0755',
    source  => '/etc/profile.d/easybuild.sh',
    require => File['install.py'],
  }

  file { 'softwares.yaml':
    ensure  => present,
    path    => "${install_dir}/softwares.yaml",
    owner   => 'sw',
    mode    => '0755',
    content => template('easybuild/softwares.yaml.erb'),
    require => File['variables.sh'],
  }

  # only run install if something has changed
  exec { 'install_softwares':
    user        => 'sw',
    path        => $easybuild::params::path,
    command     => "${var_src} && ${python} install.py ${easybuild::branch} ${easybuild::easybuild_version}'",
    umask       => '022',
    cwd         => $install_dir,
    environment => 'HOME=/home/sw',
    refreshonly => true,
    timeout     => '0',
    subscribe   => File['softwares.yaml'],
  }
}
