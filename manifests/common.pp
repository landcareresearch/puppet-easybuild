# @summary Base class to be inherited by the other easybuild classes
#
class easybuild::common {

  # Load the variables used in this module. Check the easybuild-params.pp file
  require easybuild::params

  Exec { path => $easybuild::params::path }

  $local_owner = $easybuild::ensure ? {
    'present' => 'sw',
    'absent'  => 'root',
  }

  $directoryState = $easybuild::ensure ? {
    'present' => 'directory',
    'absent'  => 'absent',
  }

  $package_state = $easybuild::ensure ? {
    'present' => 'latest',
    'absent'  => 'purged',
  }

  if $easybuild::ensure == 'present' {

    $bootstrap_file = 'bootstrap_eb.py'
    $bootstrap_url = "https://raw.githubusercontent.com/hpcugent/easybuild-framework/develop/easybuild/scripts/${bootstrap_file}"

    ensure_packages($easybuild::params::required_packages)

    exec { 'install-easybuild':
      user    => $local_owner,
      command => "bash -c 'source ${easybuild::params::module_source} &&\
 cd /tmp && wget ${bootstrap_url} && python ${bootstrap_file}\
 /opt/apps/EasyBuild && rm ${bootstrap_file}'",
      creates => '/opt/apps/EasyBuild',
      umask   => '022',
      require => [ File[ '/opt' ],
        User[ 'sw' ],
        Package[ $easybuild::params::module_package ]
      ],
    }

    # Configure environment variables for the module command
    file { '/etc/profile.d/easybuild.sh':
      ensure  => file,
      owner   => 'sw',
      source  => '/tmp/eb_config/easybuild.sh',
      require => Exec[ 'Git' ],
    }

    exec { 'Git':
      user    => 'sw',
      command => "bash -c 'cd /tmp/eb_config && git checkout\
 ${easybuild::params::branch} && git pull origin ${easybuild::params::branch}'",
      require => Exec[ 'GitInit' ],
    }

    exec { 'GitInit':
      user    => 'sw',
      command => "bash -c 'cd /tmp/eb_config &&\
 git clone https://github.com/sylmarien/easybuild-config.git . &&\
 git checkout ${easybuild::params::branch}'",
      creates => '/tmp/eb_config/.git',
      require => File[ '/tmp/eb_config' ],
    }

  } else {

    notify { 'easybuild-directory':
      message => "The /opt/apps/EasyBuild directory has not been removed\
 since it may contain some user files.",
    }

    file { '/etc/profile.d/easybuild.sh':
      ensure  => absent,
    }
  }

  file { '/opt':
    ensure  => directory,
    owner   => $local_owner,
    require => User[ 'sw' ],
  }

  user { 'sw':
    ensure     => $easybuild::ensure,
    home       => '/home/sw',
    managehome => true,
    shell      => '/bin/bash',
  }

  package { $easybuild::params::module_package:
    ensure          => $package_state,
    install_options => $easybuild::params::install_options,
  }

  file { '/tmp/eb_config':
    ensure  => directory,
    owner   => $local_owner,
    require => User[ 'sw' ],
  }
}
