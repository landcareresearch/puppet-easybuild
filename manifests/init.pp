# @summary The Puppet Module to Install and Configure Easy Build
# 
# @example
#
#   import easybuild
#
# You can then specialize the various aspects of the configuration,
# for instance:
#
#         class { 'easybuild':
#             ensure => 'present'
#         }
#
class easybuild(
  $ensure            = $easybuild::params::ensure,
  $softwares         = $easybuild::params::softwares,
  $branch            = 'core',
  $easybuild_version = ''
)
inherits easybuild::params {
  info ("Configuring easybuild (with ensure = ${ensure})")

  if !($ensure in [ 'present', 'absent' ]) {
    fail(
  "easybuild 'ensure' parameter must be set to either 'absent' or 'present'")
  }

  contain easybuild::install
  contain easybuild::config

  Class['easybuild::install'] -> Class['easybuild::config']

}
