# @summary Managing the installation of packages.
#
class easybuild::install {

  ensure_packages($easybuild::params::required_packages)

  package { $easybuild::params::module_package:
    ensure          => latest,
    install_options => $easybuild::params::install_options,
    require         => Package[$easybuild::params::required_packages],
  }

  package { $easybuild::params::py_yaml:
    ensure  => installed,
    require => Package[$easybuild::params::module_package],
  }
}
